import re

import emoji
import numpy as np
from hazm import Normalizer, word_tokenize
from keras.preprocessing import sequence
from keras.utils.np_utils import to_categorical

import config

normalizer = Normalizer()
model_config = config.ModelConfig


def extract_emojis(str):
    return [c for c in str if c in emoji.UNICODE_EMOJI]


def textclean(str):
    try:
        temp = str  # .decode('utf-8')
        temp = temp.replace("-", " ")
        temp = temp.replace("_", " ")
        temp = temp.replace("\n", " ")
        temp = temp.replace("\t", " ")
        temp = temp.replace("/", " ")
        temp = re.sub('#', ' ', temp)
        temp = re.sub('[^\u0600-\u06FF-! \u200c\n/]', ' ', temp)
        temp = normalizer.normalize(temp)
    except:
        temp = 'nan'
    return word_tokenize(temp)


def data_spliter(learning_dict):
    training_dict = {}
    VALIDATION_SPLIT = 0.2

    indices = np.arange(learning_dict["messages_char_id"].shape[0])
    np.random.shuffle(indices)
    id_char = learning_dict["messages_char_id"][indices]
    id_word = learning_dict["messages_word_id"][indices]
    labels = np.array(learning_dict["messages_label_id"])[indices]

    nb_validation_samples = int(VALIDATION_SPLIT * learning_dict["messages_char_id"].shape[0])

    training_dict["char_train"] = id_char[:-nb_validation_samples]
    training_dict["word_train"] = id_word[:-nb_validation_samples]
    training_dict["y_train"] = labels[:-nb_validation_samples]

    training_dict["char_val"] = id_char[-nb_validation_samples:]
    training_dict["word_val"] = id_word[-nb_validation_samples:]
    training_dict["y_val"] = labels[-nb_validation_samples:]
    training_dict["num_classes"] = labels.max()

    return training_dict


def token_to_id(text, dictionary, max_length=model_config.MAX_SENT_LENGTH):
    data = np.zeros((len(text), max_length), dtype='int32')
    for i, message in enumerate(text):
        for j, word in enumerate(message):
            if j < max_length:
                try:
                    data[i, j] = dictionary.token2id[word]
                except:
                    data[i, j] = 1

    return data


def char_to_id(text, dictionary, max_length=model_config.MAX_SENT_CHAR_LENGTH):
    messages_chars_id = []
    for message in text:
        message_chars_id = []
        for j, ch in enumerate(message):
            if j < model_config.MAX_SENT_CHAR_LENGTH:
                if ch in dictionary.token2id:
                    message_chars_id.append(dictionary.token2id[ch])
                else:
                    message_chars_id.append(1)
        messages_chars_id.append(message_chars_id)
    chars_id_pad = sequence.pad_sequences(messages_chars_id, maxlen=max_length)
    return chars_id_pad


def label_to_id(labels, dictionary):
    labels_id = []
    for l in labels:
        labels_id.append(dictionary.token2id[l])
    # labels_id = to_categorical(labels_id, len(dictionary))

    return labels_id
