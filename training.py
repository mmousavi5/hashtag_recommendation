#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
##########################################################
import argparse
import datetime
import logging
import os
import pickle

from keras.callbacks import TensorBoard, ModelCheckpoint

import config
import data_utils
import em_mat
import load_data
import model as Model

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

os.makedirs('./data/models', exist_ok=True)
os.makedirs('./data/pickles', exist_ok=True)

training_config = config.TrainingConfig()
read_from_pickle_flag = False


def training(corpus_path, hashtag_freq_path, w2v_path=None):
    # LOAD DATA
    if read_from_pickle_flag:
        logger.info('load data from pickle')
        with open('./data/pickles/pickle_for_training.pickle', 'rb') as handel:
            training_dict, learning_dict = pickle.load(handel)
    else:
        # LOAD DATA
        processed_docs = load_data.load_data(corpus_path, hashtag_freq_path)
        # CREATE EMBEDDING MAT

        learning_dict = em_mat.em_mat(processed_docs, w2v_path, em_mat_flag= True)
        training_dict = data_utils.data_spliter(learning_dict)

        with open('./data/pickles/pickle_for_training.pickle', 'wb') as handel:
            pickle.dump([training_dict, learning_dict],
                        handel, protocol=pickle.HIGHEST_PROTOCOL)

    # -----------------------------------------------------------------------------------------------------------------------

    # region char model

    # CREATE MODEL
    model = Model.word_char_model(len(learning_dict["dictionary_char"]), len(learning_dict["dictionary_word"]),
                                  training_dict["num_classes"]+1, learning_dict['embedding_matrix'])

    # CREATE TENSORBOARD
    tensorboard = TensorBoard(log_dir='data/tensorboard/' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S") + "/",
                              histogram_freq=0,
                              write_graph=True, write_images=False)

    save_model_filepath = 'data/models/last_model_word.hdf5'

    Model_Save_Checkpoint = ModelCheckpoint(filepath=save_model_filepath, monitor='val_loss',
                                            verbose=0, save_best_only=True, mode='auto')

    model.compile(optimizer=training_config.optimizer, loss=training_config.loss,
                  metrics=training_config.metrics)
    model.fit([training_dict["char_train"], training_dict["word_train"]], training_dict["y_train"],
              validation_data=([training_dict["char_val"], training_dict["word_val"]], training_dict["y_val"]),
              epochs=training_config.epoches, batch_size=training_config.batch_size,
              callbacks=[tensorboard, Model_Save_Checkpoint], shuffle=True)

    # endregion


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, default=None, help="Input word corpus")
    parser.add_argument("-e", "--em_mat", type=str, default=None, help="em_mat path")
    parser.add_argument("-f", "--hfp", type=str, default=None, help="hashtag freq path")
    parser.add_argument("-r", "--read_pickle", type=str, default=None, help="read from pickle")
    args = parser.parse_args()
    if int(args.read_pickle) == 1:
        read_from_pickle_flag = True
    training(args.input, args.hfp, args.em_mat)
