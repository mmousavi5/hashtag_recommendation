#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
##########################################################
from keras.optimizers import RMSprop

config = {}


class TrainingConfig(object):
    optimizer = RMSprop(lr=1e-3)
    """sgd = SGD
    rmsprop = RMSprop
    adagrad = Adagrad
    adadelta = Adadelta
    adam = Adam
    adamax = Adamax
    nadam = Nadam
    """
    loss = 'sparse_categorical_crossentropy'
    metrics = ['accuracy']
    batch_size = 1024

    p = 0.5
    base_rate = 1e-2
    momentum = 0.9
    decay_step = 15000
    decay_rate = 0.95
    epoches = 2000
    evaluate_every = 100
    checkpoint_every = 100


class ModelConfig(object):
    MAX_SENT_LENGTH = 100
    MAX_SENT_CHAR_LENGTH = 300
    MAX_SEQUENCE_LENGTH = 100
    MAX_EMOJI_LENGTH = 10
    MAX_HASHTAG_LENGTH = 10
    MAX_SENTS = 15
    MAX_NB_WORDS = 20000
    EMBEDDING_DIM = 100
    EMOJI_EMBEDDING_DIM = 100
    VALIDATION_SPLIT = 0.2
    char_index_len = 100
    CHAR_EMBEDDING_DIM = 100
    word_index_len = 200000
