import keras
from keras import regularizers
from keras.layers import (Dense, Dropout, Embedding, Input, Bidirectional, BatchNormalization, CuDNNGRU)

import config


def word_char_model(char_index_len, word_index_len, num_classes, embedding_matrix=None):
    model_config = config.ModelConfig()
    drop = 0.5
    # region word

    embedding_layer_word = Embedding(word_index_len + 1,
                                     model_config.EMBEDDING_DIM,
                                     weights=[embedding_matrix],
                                     input_length=model_config.MAX_SEQUENCE_LENGTH,
                                     trainable=False,
                                     name='embedding_layer_word')
    # cnn_gru
    sequence_input_word = Input(
        shape=(model_config.MAX_SEQUENCE_LENGTH,), dtype='int64', name='sequence_input_word')
    embedded_sequences_word = embedding_layer_word(sequence_input_word)
    word_gru_1 = Bidirectional(
        CuDNNGRU(units=150, return_sequences=True, name='word_gru1', kernel_regularizer=regularizers.l2(0.001)))(
        embedded_sequences_word)
    batch_norm_1 = BatchNormalization()(word_gru_1)
    word_gru_2 = Bidirectional(CuDNNGRU(units=150, kernel_regularizer=regularizers.l2(0.001)), name='word_gru2')(
        batch_norm_1)
    batch_norm_2 = BatchNormalization()(word_gru_2)
    char_drop = Dropout(drop, name='word_drop')(batch_norm_2)
    word_dense = Dense(2 * num_classes, activation='relu', name='word_dense',
                       kernel_regularizer=regularizers.l2(0.001))(char_drop)

    # endregion

    # region char
    embedding_layer_char = Embedding(char_index_len + 1,
                                     model_config.CHAR_EMBEDDING_DIM,
                                     input_length=model_config.MAX_SENT_CHAR_LENGTH,
                                     trainable=True,
                                     name='embedding_layer_char')

    sequence_input_char = Input(
        shape=(model_config.MAX_SENT_CHAR_LENGTH,), dtype='int64', name='sequence_input_char')
    embedded_sequences_char = embedding_layer_char(sequence_input_char)
    char_gru_1 = Bidirectional(
        CuDNNGRU(units=150, return_sequences=True, name='char_gru1', kernel_regularizer=regularizers.l2(0.001)))(
        embedded_sequences_char)
    batch_norm_3 = BatchNormalization()(char_gru_1)
    char_gru_2 = Bidirectional(CuDNNGRU(units=150, kernel_regularizer=regularizers.l2(0.001)), name='char_gru2')(
        batch_norm_3)
    batch_norm_2 = BatchNormalization()(char_gru_2)
    char_drop = Dropout(drop, name='char_drop')(batch_norm_2)
    char_dense = Dense(2 * num_classes, activation='relu', name='char_dense',
                       kernel_regularizer=regularizers.l2(0.001))(char_drop)
    # endregion

    # region concat
    concatenate = keras.layers.Concatenate(axis=-1)(
        [word_dense, char_dense])  # , dens_10_GRU , dens_10_bow, dens_10_char])
    message_embedding = keras.layers.Dense(2 * num_classes, activation='relu', kernel_regularizer=regularizers.l2(0.001),
                                           name='message_embedding')(
        concatenate)
    out_model = keras.layers.Dense(num_classes, activation='softmax')(message_embedding)
    # end region

    model = keras.models.Model(
        inputs=[sequence_input_char, sequence_input_word],
        outputs=out_model)

    model.summary()

    return model
