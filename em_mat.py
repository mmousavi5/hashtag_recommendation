#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
##########################################################

import logging
import pickle

from gensim import corpora
from gensim.models.word2vec import Word2Vec

import load_data
from data_utils import *

model_config = config.ModelConfig

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def em_mat(processed_docs, path, em_mat_flag):
    # region word2vec
    words_list = []
    learning_dict = {}

    if not em_mat_flag:

        word_vectors = Word2Vec.load(path)
        for word, vocab_obj in word_vectors.wv.vocab.items():
            if vocab_obj.count > 5:
                words_list.append([word])

        learning_dict["dictionary_word"] = corpora.Dictionary([['pad'], ['unknown']])
        learning_dict["dictionary_word"].add_documents(words_list)

        # region embedding matrix
        learning_dict['embedding_matrix'] = np.zeros(
            (len(learning_dict["dictionary_word"]) + 1, model_config.EMBEDDING_DIM))
        oov_words = 0
        for i, word in learning_dict["dictionary_word"].items():
            try:
                learning_dict['embedding_matrix'][i] = word_vectors[word]
            except:
                oov_words += 1
                continue
        logger.info('{} of word not found'.format(oov_words))
        # endregion
    else:
        with open(path, 'rb') as handel:
            temp= pickle.load(handel)
            learning_dict["dictionary_word"] = temp["dictionary_word"]
            learning_dict['embedding_matrix'] = temp['embedding_matrix']
            del temp
            # endregion
    messages_chars = [message_char[0] for message_char in processed_docs]
    messages_words = [message_char[1] for message_char in processed_docs]
    messages_label = [message_char[2] for message_char in processed_docs]
    # char dict
    learning_dict["dictionary_char"] = corpora.Dictionary([['pad'], ['unknown']])
    learning_dict["dictionary_char"].add_documents(messages_chars)

    # label dict
    learning_dict["dictionary_labels"] = corpora.Dictionary([messages_label])
    # features to id
    learning_dict["messages_char_id"] = char_to_id(messages_chars, learning_dict["dictionary_char"])
    learning_dict["messages_word_id"] = token_to_id(messages_words, learning_dict["dictionary_word"])
    learning_dict["messages_label_id"] = label_to_id(messages_label, learning_dict["dictionary_labels"])

    return learning_dict


if __name__ == '__main__':
    labels, texts, text_char = load_data.load_data()
    embedding_matrix, id_seq, id_char, len_dictionary, len_dictionary_char = em_mat(texts, text_char)
