#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
##########################################################
from __future__ import unicode_literals

import logging
import multiprocessing as mp
import signal

import numpy as np
import pandas as pd
from hazm import Normalizer
from tqdm import tqdm


def initializer():
    """Ignore CTRL+C in the worker process."""
    signal.signal(signal.SIGINT, signal.SIG_IGN)


normalizer = Normalizer()
logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(name)s : %(levelname)s : %(message)s', level=logging.DEBUG)


def extract_hash_tags(s):
    """
    extract hashtag from message
    :param s:  message
    :return: set of hashtags or nan
    """
    try:
        result = list(set(part[1:] for part in s.split() if part.startswith('#')))
        return result
    except:
        return np.nan


def pool_process(df):
    df = pd.DataFrame({
        'char': df.text.repeat(df.hashtags.str.len()).apply(lambda x: list(x)),
        'token': df.text.repeat(df.hashtags.str.len()).apply(lambda x: x.split(" ")),
        'hashtag': df.hashtags.sum()
    })
    return df


def load_data(file_path, hashtag_freq_path):
    """
    load data from csv
    :return: tuple of message characters and hashtags characters
    """
    tqdm.pandas(desc="extract hashtags")
    logger.info("loading dataset ....")

    df = pd.read_json(file_path)
    df.columns = ["id", "text"]
    df_freq = pd.read_csv(hashtag_freq_path, names=["hashtag", "freq"], header=0)
    freq_dict = dict([tuple(x) for x in df_freq[df_freq.freq > 35].values])
    # df = df.head(1000)
    df["hashtags"] = df.progress_apply(lambda x: extract_hash_tags(x["text"]), axis=1)
    tqdm.pandas(desc="remove message that include frequent hashtags")
    df["hashtags_len"] = df.hashtags.progress_apply(lambda x: len(x))
    df = df[(df.hashtags_len < 5) & (df.hashtags_len > 0)]
    logger.info("create tuple of message chars ,message token and hashtags")
    p = mp.Pool(7, initializer=initializer)
    df = pd.concat(p.map(pool_process, np.array_split(df, 7)))
    p.close()
    # frequency table of hashtags
    logger.info("remove rare hashtag")
    # pd.DataFrame(df.hashtag.value_counts()).reset_index().to_csv("./hashtag_frequency_table.csv", encoding="utf-8",
    #                                                              index=False)

    # remove frequent and rar hashtags
    df["rar"] = df.hashtag.apply(lambda x: freq_dict.get(x, np.nan))
    df = df[pd.notna(df.rar)]
    # for name, group in df.groupby('hashtag'):
    #     if len(group) < 35 or len(group) > 20000:
    #         df = df[df['hashtag'] != name]

    processed_docs_df = [tuple(x) for x in df[['char', 'token', 'hashtag']].values]
    logger.info(
        'curpos size =\tsentence size = {}'.format(len(processed_docs_df)))
    return processed_docs_df


if __name__ == '__main__':
    processed_docs = load_data("./data/random_year")
